# MachineKit Encoder

## Motivation

I want an encoder! Encoders<sup>1</sup> read position, generally by counting 'ticks' on some reference. For example, an Optical Encoder uses a reference with openings that allow light to pass, and count whenever a photodiode goes high or low to indicate that some motion has occurred.

![encoder-basically](https://gitlab.cba.mit.edu/jakeread/mkencoder/raw/master/images/encoder-basically.jpg)

Then, you add the ticks together to get your position. In order to detect direction, a technique called 'quadrature' is used:

![quadrature-basically](https://gitlab.cba.mit.edu/jakeread/mkencoder/raw/master/images/quadrature-basically.png)

Super simple, very neat. Here's picture of a *fancy* encoder:

![encoder-fancy](https://gitlab.cba.mit.edu/jakeread/mkencoder/raw/master/images/encoder-fancy.jpg)

You'll notice this encoder has two other reference markings - those are super interesting as well, and encode *absolute* position into the ring - there is only one uniqe combination of holes-or-not-holes on those rings for every single indicated position. This is good news because then your joint / axis does not have to touchoff on a known reference point - aka a 'home' or 'limit' switch - before it starts counting. If this looks expensive, it's because it is. An encoder like this might run ~ $300, and they're super sensitive to dust, misalignment etc. That's no bueno.

**So** my goal here is develop an encoder that I can put on whatever-I-want without spending too much money, and without fussing too much about setup. Critically, I also want to un-constrain myself from particular sizes - whenever I have to source a particular size ring or strip (for reference) I end up boxed-in in terms of the *~ design spaces ~* I can get into. I don't like that.

## Solution: Capacitive Encoder

[Go Here](https://gitlab.cba.mit.edu/jakeread/mkencoder/tree/master/capacitive.md) for Capacitive Writeup and Doc

## Solution: Magnetic Encoder

[Go Here](https://gitlab.cba.mit.edu/jakeread/mkencoder/tree/master/magnetic.md) for Magnetic Writeup and Doc