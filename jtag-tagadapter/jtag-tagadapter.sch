<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.3.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="fab">
<packages>
<package name="2X5_1.27MM_SMD">
<smd name="1" x="-1.6" y="2.54" dx="2.286" dy="0.254" layer="1"/>
<smd name="3" x="-1.6" y="1.27" dx="2.286" dy="0.254" layer="1"/>
<smd name="5" x="-1.6" y="0" dx="2.286" dy="0.254" layer="1"/>
<smd name="7" x="-1.6" y="-1.27" dx="2.286" dy="0.254" layer="1"/>
<smd name="9" x="-1.6" y="-2.54" dx="2.286" dy="0.254" layer="1"/>
<smd name="10" x="1.6" y="-2.54" dx="2.286" dy="0.254" layer="1" rot="R180"/>
<smd name="8" x="1.6" y="-1.27" dx="2.286" dy="0.254" layer="1" rot="R180"/>
<smd name="6" x="1.6" y="0" dx="2.286" dy="0.254" layer="1" rot="R180"/>
<smd name="4" x="1.6" y="1.27" dx="2.286" dy="0.254" layer="1" rot="R180"/>
<smd name="2" x="1.6" y="2.54" dx="2.286" dy="0.254" layer="1" rot="R180"/>
<wire x1="-1.715" y1="3.175" x2="-1.715" y2="-3.175" width="0.127" layer="51"/>
<wire x1="-1.715" y1="-3.175" x2="1.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.715" y1="-3.175" x2="1.715" y2="3.175" width="0.127" layer="51"/>
<wire x1="1.715" y1="3.175" x2="-1.715" y2="3.175" width="0.127" layer="21"/>
<circle x="-2.105" y="3.71" radius="0.3238" width="0.1016" layer="21"/>
<text x="-1.27" y="3.81" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.27" y="-5.08" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.2" y1="2.3" x2="-1.3" y2="2.8" layer="51"/>
<rectangle x1="-2.2" y1="1.02" x2="-1.3" y2="1.52" layer="51"/>
<rectangle x1="-2.2" y1="-0.25" x2="-1.3" y2="0.25" layer="51"/>
<rectangle x1="-2.2" y1="-1.52" x2="-1.3" y2="-1.02" layer="51"/>
<rectangle x1="-2.2" y1="-2.79" x2="-1.3" y2="-2.29" layer="51"/>
<rectangle x1="1.3" y1="2.3" x2="2.2" y2="2.8" layer="51"/>
<rectangle x1="1.3" y1="1.02" x2="2.2" y2="1.52" layer="51"/>
<rectangle x1="1.3" y1="-0.24" x2="2.2" y2="0.26" layer="51"/>
<rectangle x1="1.3" y1="-1.49" x2="2.2" y2="-0.99" layer="51"/>
<rectangle x1="1.3" y1="-2.78" x2="2.2" y2="-2.28" layer="51"/>
</package>
<package name="TC2050-IDC">
<smd name="1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="2" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="5.08" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="6.35" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="6" x="6.35" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="7" x="5.08" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="8" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="9" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="10" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<hole x="0" y="0" drill="0.9906"/>
<hole x="7.62" y="1.016" drill="0.9906"/>
<hole x="7.62" y="-1.016" drill="0.9906"/>
<rectangle x1="1.27" y1="-0.635" x2="6.35" y2="0.635" layer="39"/>
<hole x="0" y="2.54" drill="2.3749"/>
<hole x="5.715" y="2.54" drill="2.3749"/>
<hole x="0" y="-2.54" drill="2.3749"/>
<hole x="5.715" y="-2.54" drill="2.3749"/>
</package>
<package name="2X5_1.27MM_PTH">
<pad name="6" x="0.635" y="0" drill="0.8382" diameter="1.016"/>
<pad name="5" x="-0.635" y="0" drill="0.8382" diameter="1.016"/>
<pad name="4" x="0.635" y="1.27" drill="0.8382" diameter="1.016"/>
<pad name="3" x="-0.635" y="1.27" drill="0.8382" diameter="1.016"/>
<pad name="10" x="0.635" y="-2.54" drill="0.8382" diameter="1.016"/>
<pad name="9" x="-0.635" y="-2.54" drill="0.8382" diameter="1.016"/>
<pad name="8" x="0.635" y="-1.27" drill="0.8382" diameter="1.016"/>
<pad name="7" x="-0.635" y="-1.27" drill="0.8382" diameter="1.016"/>
<pad name="2" x="0.635" y="2.54" drill="0.8382" diameter="1.016"/>
<pad name="1" x="-0.635" y="2.54" drill="0.8382" diameter="1.016" stop="no" thermals="no"/>
<wire x1="-1.7018" y1="3.175" x2="-1.7018" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.7018" y1="-3.175" x2="1.7018" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.7018" y1="-3.175" x2="1.7018" y2="3.175" width="0.127" layer="21"/>
<wire x1="1.7018" y1="3.175" x2="-1.7018" y2="3.175" width="0.127" layer="21"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ARM_SWD_10">
<pin name="VCC" x="-17.78" y="5.08" length="middle"/>
<pin name="GND@3" x="-17.78" y="2.54" length="middle"/>
<pin name="GND@5" x="-17.78" y="0" length="middle"/>
<pin name="GND@7" x="-17.78" y="-2.54" length="middle"/>
<pin name="GND@9" x="-17.78" y="-5.08" length="middle"/>
<pin name="NRESET" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="NC/TDI" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="SWO/TDO" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="SWDCLK/TCK" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="SWDIO/TMS" x="17.78" y="5.08" length="middle" rot="R180"/>
<wire x1="-12.7" y1="7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<text x="-12.7" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-12.7" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="-12.7" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARM_10" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="ARM_SWD_10" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="2X5_1.27MM_SMD">
<connects>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GND@7" pad="7"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="NRESET" pad="10"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2x5_1.27MM_SMD"/>
</technology>
</technologies>
</device>
<device name="PTH" package="2X5_1.27MM_PTH">
<connects>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GND@7" pad="7"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="NRESET" pad="10"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2x5_1.27MM_PTH"/>
</technology>
</technologies>
</device>
<device name="TDC2050-IDC" package="TC2050-IDC">
<connects>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GND@7" pad="7"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="NRESET" pad="10"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="TC2050-IDC"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J1" library="fab" deviceset="ARM_10" device="SMD"/>
<part name="J2" library="fab" deviceset="ARM_10" device="PTH"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="J1" gate="G$1" x="81.28" y="12.7"/>
<instance part="J2" gate="G$1" x="81.28" y="-15.24"/>
</instances>
<busses>
</busses>
<nets>
<net name="1" class="0">
<segment>
<wire x1="63.5" y1="-10.16" x2="53.34" y2="-10.16" width="0.1524" layer="91"/>
<label x="53.34" y="-10.16" size="1.778" layer="95"/>
<label x="53.34" y="-10.16" size="1.778" layer="95"/>
<label x="53.34" y="-10.16" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="VCC"/>
<wire x1="63.5" y1="17.78" x2="58.42" y2="17.78" width="0.1524" layer="91"/>
<label x="58.42" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="2" class="0">
<segment>
<wire x1="63.5" y1="-12.7" x2="53.34" y2="-12.7" width="0.1524" layer="91"/>
<label x="53.34" y="-12.7" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="GND@3"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="SWDIO/TMS"/>
<wire x1="99.06" y1="17.78" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<label x="99.06" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="3" class="0">
<segment>
<wire x1="63.5" y1="-15.24" x2="53.34" y2="-15.24" width="0.1524" layer="91"/>
<label x="53.34" y="-15.24" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="GND@5"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND@3"/>
<wire x1="63.5" y1="15.24" x2="58.42" y2="15.24" width="0.1524" layer="91"/>
<label x="58.42" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="4" class="0">
<segment>
<wire x1="63.5" y1="-17.78" x2="53.34" y2="-17.78" width="0.1524" layer="91"/>
<label x="53.34" y="-17.78" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="GND@7"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="SWDCLK/TCK"/>
<wire x1="99.06" y1="15.24" x2="104.14" y2="15.24" width="0.1524" layer="91"/>
<label x="99.06" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="5" class="0">
<segment>
<wire x1="63.5" y1="-20.32" x2="53.34" y2="-20.32" width="0.1524" layer="91"/>
<label x="53.34" y="-20.32" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="GND@9"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND@5"/>
<wire x1="63.5" y1="12.7" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<label x="58.42" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="10" class="0">
<segment>
<wire x1="99.06" y1="-10.16" x2="106.68" y2="-10.16" width="0.1524" layer="91"/>
<label x="101.6" y="-10.16" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="SWDIO/TMS"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="NRESET"/>
<wire x1="99.06" y1="7.62" x2="104.14" y2="7.62" width="0.1524" layer="91"/>
<label x="99.06" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="9" class="0">
<segment>
<wire x1="99.06" y1="-12.7" x2="106.68" y2="-12.7" width="0.1524" layer="91"/>
<label x="101.6" y="-12.7" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="SWDCLK/TCK"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND@9"/>
<wire x1="63.5" y1="7.62" x2="58.42" y2="7.62" width="0.1524" layer="91"/>
<label x="58.42" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="8" class="0">
<segment>
<wire x1="99.06" y1="-15.24" x2="106.68" y2="-15.24" width="0.1524" layer="91"/>
<label x="101.6" y="-15.24" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="SWO/TDO"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="NC/TDI"/>
<wire x1="99.06" y1="10.16" x2="104.14" y2="10.16" width="0.1524" layer="91"/>
<label x="99.06" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="7" class="0">
<segment>
<wire x1="99.06" y1="-17.78" x2="106.68" y2="-17.78" width="0.1524" layer="91"/>
<label x="101.6" y="-17.78" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="NC/TDI"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND@7"/>
<wire x1="63.5" y1="10.16" x2="58.42" y2="10.16" width="0.1524" layer="91"/>
<label x="58.42" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="6" class="0">
<segment>
<wire x1="99.06" y1="-20.32" x2="106.68" y2="-20.32" width="0.1524" layer="91"/>
<label x="101.6" y="-20.32" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="NRESET"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="SWO/TDO"/>
<wire x1="99.06" y1="12.7" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<label x="99.06" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
