# Solution: Capacitive Encoder

Here, I'll try PCB-fabbing a reference circle with triangular teeth, and two pads to read through.

As the ring moves, capacitance should vary between the pads - I can read this as a value on an ADC pin and track that to figure position.

## Commercial Sensors

I'm dubious about Neils TX / RX settle time - 100us (yikes!). I'm looking into using some other IC's - drivers that are made for this - to do the conversion.

TI has two - the FDC1004 and the FDC2112 - both are I2C devices. The FDC2112, rather than doing rise / delay, watches deviation in resonant frequency at the sensor location. Oddly, none that I can find use Neils TX / RX Scheme for environmental cancelling, all use TX / GND.

In short, these solutions seem cool, but complicated, and I need to take some more time with the bare-bones of this problem to determine if I can make something work. Like,
 - does rise-time drop with smaller pads? less charge to distribute...
 - how many up/down sets do I need for a reasonable measurement?
 - adc resolution -> bar width / spacing
 - grounding, stack setup, tx pads, rx pad(s)?

## Understanding the Phenomenon

It's good to try to get a grounding on what's really going on. To get the basics in order, I go [here](http://hyperphysics.phy-astr.gsu.edu/hbase/electric/pplate.html). 

Capacitance is $`C = \frac{k * e * A}{d}`$ where $'k'$ is the relative permittivity of the material between the plates, $'e'$ is a constant making $'k * e = 1'$ in air, $'A'$ is the area of the plates and $'d'$ is the separation of those plates. 

Some quick thoughts 
 - do static tx / rx pads. reference is metallic thing that moves between them, changing the permeattivity of space between them
  - bc capacitance is C = \frac{k * e * A}{d} where E is constant, A is area, and K is relative permeattivity (k ~= 1 for air)
  - 'metallic thing' can be double-sided pcb w/ sawtooth or sine pattern to read
 - can do spreadsheet maths for tx / rx areas, see what total capacitance range will be (1pf -> 15pf?)
 - can figure apparent rise-time calculator, probably?

http://hyperphysics.phy-astr.gsu.edu/hbase/electric/pplate.html

http://www.ti.com/lit/an/snoa927/snoa927.pdf

To quickly lay this out, I'll take the linear case... and a triangular wave to read.

 - notebook pic

 - or whiteboard, do pitch, do 1/4 -> 1/2 -> 3/4

So I can build a quick spreadsheet for this. Using basic areas and http://hyperphysics.phy-astr.gsu.edu/hbase/electric/pplate.html 

C = k*e_0*A/d

What changes as the imaged plate moves is the permittivity of the space between the tx and rx pads. Copper has a technically infinite permittivity, so in some sense what we're doing is decreasing the space between the pads, when the copper passes through. *shrugman*